import Header from './component/Header'
import Body from './component/Body'


function App() {
  return (
    <div className="App container">
      <Header />
      <Body/>
    </div>
  );
}

export default App;
