import React, { Fragment, useState } from "react";
import Card from "./Card"
import {
    MDBCard,
    MDBCardBody,
    MDBCardTitle,
    MDBCardText,
} from 'mdb-react-ui-kit';
import "../css/body.css"
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';

const NameData = ["Payments API", "Checkout", "Dashboard", "Payment Link"]
const barData = [{ state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "downtime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "downtime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "downtime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "downtime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" },
{ state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "downtime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "partial", date: "some date" }, { state: "downtime", date: "some date" }, { state: "uptime", date: "some date" }, { state: "uptime", date: "some date" }]


const Body = () => {

    let [state, setState] = useState(barData)
    const filterFunc = (e) => {
        if (e.target.checked) {
            const newData = state.filter((data) => {
                if (data.state === "downtime") {
                    return data
                }
                return
            })
            return setState(newData)
        }
        return setState(barData)
    }

    return (
        <Fragment>

            <div className="body container">
                <h1>Razorpay Status Page</h1>
                <p style={{ padding: "2% 10% 0 10%" }}>Razorpay status page publishes the most up-to-the-minute information on product availability. Check back here any time to get current status/information on individual products. If you are experiencing a real-time, operational issue with one of our products that is not described below, please reach out to <u>our support team </u>and we will help you out.</p>
                <MDBCard className="mainCard container" >
                    <MDBCardBody>
                        <MDBCardTitle><span style={{ color: "rgba(31,40,73,.87)", fontSize: "25px", padding: "3%" }}> Razorpay Payments <FormControlLabel control={<Switch defaultChecked={false} onChange={filterFunc} />} label="DownTime" /></span>
                        </MDBCardTitle>
                        <MDBCardText>
                            {
                                NameData.map((data, index) => {
                                    return <Card name={data} bardata={state} key={index} />
                                })
                            }
                        </MDBCardText>
                    </MDBCardBody>
                    <div id="bar-index">
                        <div className="card-footer">
                            <div id="uptime-box">
                            </div>
                            100% Uptime
                        </div>
                        <div className="card-footer">
                            <div id="partial-box" >
                            </div>
                            Partial Degradation
                        </div>
                        <div className="card-footer">
                            <div id="downtime-box">
                            </div>
                            Downtime
                        </div>
                    </div>
                </MDBCard>
            </div>
        </Fragment>

    )
}

export default Body