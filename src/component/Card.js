import React from "react";
import {
    MDBCard,
    MDBCardBody,
} from 'mdb-react-ui-kit';
import Tooltip from '@mui/material/Tooltip';
import "../css/card.css"



const Card = (props) => {
    let barClass;
    let toolTipValue;
    const barData = props.bardata
    return (
        <div className="container" style={{ textAlign: "left"}}>
            <MDBCard className="card rendingCard">
                <MDBCardBody>
                    <div className="header-tab">
                        <div>{props.name}
                            <Tooltip placement="right" title="Razorpay's core payment infra structure" arrow>
                              <img alt="info" src="https://img.icons8.com/material-outlined/20/000111/info--v3.png" style={{marginLeft:"10px"}}/>
                            </Tooltip>

                        </div>
                        <input type="button" className="online-btn" value="Online" />
                    </div>
                    <div className="bar">
                        {
                            barData.map((data,index) => {
                                if (data.state === 'partial') {
                                    barClass = "partial-bar"
                                    toolTipValue = `${data.date} \n Degraded for 50 minutes`
                                } else if (data.state === 'downtime') {
                                    barClass = "downtime-bar "
                                    toolTipValue = `${data.date} \n Downtime`
                                } else {
                                    barClass = "uptime-bar"
                                    toolTipValue = `${data.date} \n No Downtime`
                                }

                                return (
                                    <Tooltip title={toolTipValue} arrow key={index} >
                                        <div className={barClass}></div>
                                    </Tooltip>
                                )
                            })}
                    </div>
                    <span className="para-text">status over the past 90 days</span>
                    <div className='recent-incident'>No recent incidents</div>
                </MDBCardBody>
            </MDBCard>
        </div>
    )
}

export default Card